path = require('path')
TelegramBot = require('telegram-bot-node').Bot

horobot = new TelegramBot(process.env.TELEGRAM_TOKEN,
  name: 'Horobot'
  polling: true
  plugins: path.resolve(__dirname, './plugins/'))

horobot.on 'message', (msg) ->
  console.log "Message from #{msg.from.username}: #{msg.text}"
  horobot.handle msg
