fs      = require 'fs'
mkdirp  = require 'mkdirp'
moment  = require 'moment'
request = require 'request'
cheerio = require 'cheerio'
iconv   = require 'iconv-lite'
_       = require 'lodash'

signs = require './signs'

extract = (body) ->
  $ = cheerio.load iconv.decode(body, 'win1251'), { decodeEntities: false }
  ps = $('.oculus-epz-text p').map -> $(this).html()
  ps.get().join("\n")

module.exports = (sign, cb) ->
  mkdirp.sync "./cache"
  cacheFile = "./cache/#{moment().format('YYYYMMDD')}.json"

  horoscopes = if fs.existsSync(cacheFile)
    JSON.parse(fs.readFileSync(cacheFile))
  else {}

  if horoscopes[sign] then return cb(null, horoscopes)

  returned = 0

  _.each signs.oculus, (sign) ->
    opts =
      url: "http://www.oculus.ru/goroskop_na_segodnya/#{sign}.html"
      encoding: null

    request opts, (err, response, body) ->
      if err then return err
      horoscopes[sign] = extract(body)

      if ++returned is signs.oculus.length
        fs.writeFileSync(cacheFile, JSON.stringify(horoscopes))
        return cb(null, horoscopes)

