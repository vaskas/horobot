vow           = require 'vow'
MESSAGE_TYPES = require('telegram-bot-node').MESSAGE_TYPES

module.exports =
  type: MESSAGE_TYPES.COMMAND
  weight: 1
  test: (info) ->
    info.data.command is 'start'

  handler: (info, bot) ->
    deferred = vow.defer()

    bot.sendMessage "Привет! Чтобы узнать гороскоп, просто пришли мне свой знак зодиака"
      .then (resp) ->
        deferred.resolve(resp)
      , (reason) ->
        deferred.reject(reason)

    deferred.promise()

