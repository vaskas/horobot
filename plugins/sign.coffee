moment   = require 'moment'
vow      = require 'vow'
download = require '../lib/download'
signs    = require '../lib/signs'

MESSAGE_TYPES = require('telegram-bot-node').MESSAGE_TYPES
moment.locale('ru')

module.exports =
  type: MESSAGE_TYPES.TEXT
  weight: 1
  test: (info) ->
    signs.ru.indexOf(info.data.text.toLowerCase()) isnt -1

  handler: (info, bot) ->
    signRu = info.data.text.toLowerCase()
    sign = signs.oculus[signs.ru.indexOf(signRu)]

    deferred = vow.defer()
    download sign, (err, horoscopes) ->
      if (err)
        deferred.reject(err)
      else
        bot.sendMessage "#{moment().format('D MMMM')}, #{signRu}. #{horoscopes[sign]}"
          .then (resp) ->
            deferred.resolve(resp)
          , (reason) ->
            deferred.reject(reason)

    deferred.promise()

